import argparse
import random
import traceback, sys
from subprocess import call
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import shutil
import logging
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms
from torch.autograd import Variable
import torchvision.models as models
import random
from torchvision.utils import save_image

parser = argparse.ArgumentParser(description='Autoencoders for IMR')
parser.add_argument('--workers', '-j', default=4, type=int, metavar='N', help='number of data loading workers')
parser.add_argument('--batch_size','-b', default=500, type=int, metavar='B', help='mini-batch size')
parser.add_argument('--cuda','-c', help='Use cuda to train model', default=False)
parser.add_argument('--arch', '-a', metavar='ARCH', default='resnet18', help='model architecture:')
parser.add_argument('--lr', '-lr', default=0.1, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--m', '-m', default=0.9, type=float, metavar='M', help='momentum')
parser.add_argument('--wd', '-wd', default=1e-4, type=float, metavar='W', help='weight decay')
parser.add_argument('--start_epoch', default=0, type=int, metavar='N', help='manual epoch number (useful on restarts)')
parser.add_argument('--epochs', '-e', default=2, type=int, metavar='E', help='number of total epochs to run')
parser.add_argument('--print_freq', '-p', default=10, type=int, metavar='P', help='print frequency (default: 10)')
parser.add_argument('--graph','-graph', metavar='PATH', help='graph (png) to be written to')
parser.add_argument('--randseed','-seed', default=9302, type=int, metavar='N', help='random number seed')
parser.add_argument('--model_path','-m_path', metavar='PATH', type=str, help='path of best model')
parser.add_argument('--check_path','-c_path', metavar='PATH', type=str, help='path of checkpoint model')
parser.add_argument('--traintype', '-ttype', metavar='traintype', default='scratch', help='type of training')
parser.add_argument('--dataroot','-dataroot', metavar='PATH', type=str, help='path of best model')

logging.basicConfig(level=logging.DEBUG)

best_err = 100

class autoencoder(nn.Module):
    def __init__(self):
        super(autoencoder, self).__init__()
        self.encoder = nn.Sequential(
	    
            nn.Linear(224 * 224 * 3, 128),
            nn.ReLU(True),
            nn.Linear(128, 64),
            nn.ReLU(True), nn.Linear(64, 12), nn.ReLU(True), nn.Linear(12, 3))
        self.decoder = nn.Sequential(
            nn.Linear(3, 12),
            nn.ReLU(True),
            nn.Linear(12, 64),
            nn.ReLU(True),
            nn.Linear(64, 128),
            nn.ReLU(True), nn.Linear(128, 224 * 224 * 3), nn.Tanh())

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

def main():
        global args, best_err
        args = parser.parse_args()
	manualSeed = random.randint(0,1000)#args.randseed ; 
	random.seed(manualSeed)
	torch.manual_seed(manualSeed)

        font = {'family' : 'normal',
        'size'   : 18}

        mpl.rc('font', **font)

	transformation = transforms.Compose([transforms.ToTensor(), transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.2, 0.2, 0.2])])
	kwargs={'num_workers': args.workers, 'pin_memory': True} if args.cuda==True else {'num_workers': args.workers, 'pin_memory': False}
        
	'''loaders'''
	dataset = dset.ImageFolder(root=args.dataroot,
                               transform=transforms.Compose([
			           transforms.Scale(256),
				   transforms.CenterCrop(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.2, 0.2, 0.2)),
                               ]))

	train_loader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size,
                                         shuffle=True, num_workers=args.workers)
	
	model = autoencoder()		
	criterion = nn.MSELoss()
       
	optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.wd)
 
	if args.cuda:
		model = model.cuda() ; criterion = criterion.cuda()

	fig = plt.figure(figsize=(10,10)) ;
	lines = ['m^-'] ; labels = ['Train_err'] ; ax1 = plt.subplot(1,1,1) ; 	

	tr_loss = []  ; ep = [] 
	for epoch in range(args.start_epoch, args.epochs):
		adjust_learning_rate(optimizer, epoch)
		train_loss, out = train(train_loader, model, criterion, optimizer, epoch)
		ep.append(epoch) ; 
		tr_loss.append(train_loss.avg) ; 

		is_best = train_loss.avg < best_err
		best_err = min(train_loss.avg,best_err)
		save_checkpoint({
            			'epoch': epoch,
            			'state_dict': model.state_dict(),
            			'best_err': best_err,
				'batch_size': args.batch_size,
				'rand_seed': args.randseed,
				'curves': args.graph,
            			'optimizer' : optimizer.state_dict(),
        		}, is_best)
		if is_best:
                        pic = to_img(out.cpu().data)
                        save_image(pic, 'image_{}.png'.format(epoch))


	ax1.plot(ep, tr_loss, lines[0], label=labels[0])
	plt.show()
	plt.xlabel('#epochs') ; plt.ylabel('err') ; plt.title('Perf')
	ax1.legend()
	fig.savefig(args.graph)

def to_img(x):
    x = 0.5 * (x + 1)
    x = x.clamp(0, 1)
    x = x.view(x.size(0), 3, 224, 224)
    return x

'''train'''
def train(train_loader, model, criterion, optimizer, epoch):
    	losses = AverageMeter()
	
        for batch_idx, (input,lab) in enumerate(train_loader):
		model.train()
		if args.cuda:
                	input = input.cuda()

		input = input.view(input.size(0), -1)
		input = Variable(input)
		
            	output = model(input)
            	loss = criterion(output, input)
            	losses.update(loss.data[0], input.size(0))	
		
		optimizer.zero_grad()
            	loss.backward()
            	optimizer.step()
            	print('Epoch: [{0}][{1}/{2}]\t'
                  		'Train Loss {loss.avg:.4f}'.format(
                   		epoch, batch_idx, len(train_loader),loss=losses))
	return losses, output

'''save checkpoint'''
def save_checkpoint(state, is_best, filename='checkpath_default.pth.tar'):
	filename = args.check_path
	torch.save(state, filename)
        if is_best:
        	shutil.copyfile(filename, args.model_path)

'''vary lr according to epoch'''
def adjust_learning_rate(optimizer, epoch):
	if  0 <= epoch <= 25:
		lr = args.lr * 1
	elif 26 <= epoch <=50 : 
        	lr = args.lr * 0.1
	elif 51 <= epoch <= 65:
		lr = args.lr * 0.01
	else:
		lr = args.lr * 0.001
        
	for param_group in optimizer.param_groups:
        	param_group['lr'] = lr

'''Computes and stores the average and current value'''
class AverageMeter(object):
    	def __init__(self):
        	self.reset()

    	def reset(self):
        	self.val = 0
        	self.avg = 0
        	self.sum = 0
        	self.count = 0

    	def update(self, val, n=1):
        	self.val = val
        	self.sum += val * n
        	self.count += n
        	self.avg = self.sum / self.count
		
if __name__ == '__main__':
        main()
