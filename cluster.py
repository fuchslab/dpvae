import csv
import cv2
import numpy as np
from PIL import Image
from scipy.spatial import distance
from itertools import groupby
from operator import itemgetter
import os
from sklearn.decomposition import IncrementalPCA, PCA
import argparse
import time
from joblib import Parallel, delayed
import multiprocessing
import warnings
import pandas as pd
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.preprocessing import StandardScaler
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import warnings
import random
from sklearn.manifold import TSNE

parser = argparse.ArgumentParser(description='Clustering | kMeans')

parser.add_argument('--query','-qdir', metavar='DIR', help='path to query directory')
parser.add_argument('--top_k','-k', metavar='int', type=int, help='top k best candidates for a given query')
parser.add_argument('--chunksize','-csize', metavar='int', type=int, help='chunksize for incremntal pca')
parser.add_argument('--num_cores','-ncores', metavar='int', type=int, help='chunksize for incremntal pca')
parser.add_argument('--gist_feats', '-gfeats', metavar='PATH', help='gist features csv file to be written to')
parser.add_argument('--hist_feats', '-hfeats', metavar='PATH', help='hist features csv file to be written to')
parser.add_argument('--extract_features', '-ef', metavar='PATH', help='True or False')
parser.add_argument('--result', '-res', metavar='PATH', help='result csv file to be written to')
parser.add_argument('--clusters','-clusters', metavar='int', type=int, help='# desired clusters')
parser.add_argument('--samples','-samples', metavar='int', type=int, help='# samples | picked randomly')
parser.add_argument('--graph','-graph', metavar='PATH', help='graph (jpg) to be written to')
parser.add_argument('--seed','-seed', metavar='int', type=int, help='specify seed to reproduce results')
parser.add_argument('--tsne', '-tsne', metavar='PATH', help='True (plot tsne) or False')
parser.add_argument('--n_components_L2','-n_components_L2', metavar='int', type=int, help='#n components for second level')

def extract_gist_pca_features(im, pca):
                img = Image.open(im)
                des_gist = leargist.color_gist(img)
                des_pca = pca.transform(des_gist.reshape(1,len(des_gist)))
                if isinstance(des_pca, np.ndarray):
                        return des_pca
                else:
                        return None

def extract_hist_pca_features(im, pca):
                img = cv2.imread(im) ;
                des_hist = cv2.calcHist([img],[0,1,2],None, [5, 5, 5],[0, 256, 0, 256, 0, 256])
                des_hist = cv2.normalize(des_hist).flatten()
                des_pca = pca.transform(des_hist.reshape(1,des_hist.shape[0]))
                if isinstance(des_pca, np.ndarray):
                        return des_pca
                else:
                        return None

#incremental pca 
def pca_train(fea_file):
	with warnings.catch_warnings():
		for X in pd.read_csv(fea_file,chunksize=args.chunksize,iterator=True):
			warnings.simplefilter("ignore", category=RuntimeWarning)
			X= np.array(X) ; X= np.squeeze(X) 
			X = StandardScaler().fit_transform(X)
			pca = IncrementalPCA(n_components=args.n_components_L2) ; 
			pca.partial_fit(X)	
	return pca

#gist
def extract_gist(im):
	im = args.query + im
        img = Image.open(im)

        des_gist = leargist.color_gist(img)

        des = np.reshape(des_gist, (1,len(des_gist)))
        if isinstance(des, np.ndarray):
                return des
        else:
                return None

#colorhist
def extract_hist(im):
        im = args.query + im
        img = cv2.imread(im) ;
        des_hist = cv2.calcHist([img], [0,1,2], None, [5, 5, 5], [0, 256, 0, 256, 0, 256])
	des_hist = cv2.normalize(des_hist).flatten()
        des_hist = np.reshape(des_hist, (1,len(des_hist)))
        if isinstance(des_hist, np.ndarray):
                return des_hist
        else:
                return None

#init file to write results
def res_file_write_init(result):
        res_file_name = result
        resFile = open(res_file_name, 'w')
        resWriter = csv.writer(resFile)

        return resWriter, resFile, res_file_name

def read_image_path(im,t):
	if t == 'query':
		im = args.query + im ;
	
	return im

def cluster(X, im_q_paths, writer):
	#for very large datasets switch to incremental pca
	#read from files in chunnks (pd)
	
	X_reduced_transform = X; 
	mbk = MiniBatchKMeans(n_clusters=args.clusters, n_init=1, batch_size=args.chunksize, reassignment_ratio=0.0)
	mbk.fit(X_reduced_transform)

	# get centroid-point distances 
	X_dists = mbk.transform(X_reduced_transform)	

	# predict all points and form tuples: (img_path, label)
	Z_label = mbk.predict(X_reduced_transform)
	dists = X_dists.min(axis=1) 

	if args.tsne == 'True':
		X_reduced_transform = TSNE(n_components=2).fit_transform(X_reduced_transform)
	mbk.fit(X_reduced_transform)

	imgWithlabel= [x for x in zip(im_q_paths, Z_label, dists, X_reduced_transform.tolist())]
	writer.writerow([imgWithlabel])

	# Step size of the mesh. Decrease to increase the quality of the VQ.
	h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

	# Plot the decision boundary. For that, we will assign a color to each
	x_min, x_max = X_reduced_transform[:, 0].min() - 1, X_reduced_transform[:, 0].max() + 1
	y_min, y_max = X_reduced_transform[:, 1].min() - 1, X_reduced_transform[:, 1].max() + 1
	xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

	# Obtain labels for each point in mesh. Use last trained model.
	Z = mbk.predict(np.c_[xx.ravel(), yy.ravel()])
	Z = Z.reshape(xx.shape)
	fig = plt.figure(1)
	plt.clf()
	
	plt.imshow(Z, interpolation='nearest',
           extent=(xx.min(), xx.max(), yy.min(), yy.max()),
           cmap=plt.cm.Paired,
           aspect='auto', origin='lower')
	plt.plot(X_reduced_transform[:, 0], X_reduced_transform[:, 1], 'k.', markersize=2)
	
	# Plot the centroids as a white X
	centroids = mbk.cluster_centers_
	plt.scatter(centroids[:, 0], centroids[:, 1],
            marker='x', s=169, linewidths=3,
            color='w', zorder=10)
	plt.title('K-means clustering on Cholangio dataset (PCA-reduced data)\n'
          'Centroids are marked with white cross')
	plt.xlim(x_min, x_max) ; plt.ylim(y_min, y_max) ; plt.xticks(()) ; plt.yticks(())
	
	fig.savefig(args.graph)

	return None

#main
def main():
        global args
        args = parser.parse_args()

	np.random.seed(args.seed)
	
	num_cores = args.num_cores
	pool = multiprocessing.Pool(num_cores)

	top_k =  args.top_k	
	read_q_data_orig = os.listdir(args.query)
	
	#randomly sanple from the query images directory 
	choices = np.random.choice(len(read_q_data_orig), args.samples, replace=False)
	read_q_data = [read_q_data_orig[x] for x in range(len(choices))]
	
	resW, res_file_name, resFile = res_file_write_init(args.result);

	ef = args.extract_features ;
	if (ef == 'True'):
		#extract candidate features to train pca
		print('\nExtract features from candidates to train PCA....\n')
		'''
		with open(args.gist_feats, 'w') as f:
                	for feat in pool.imap(extract_gist,read_q_data,chunksize=args.chunksize):
                        	np.savetxt(f, feat, delimiter=",")
		'''
        	with open(args.hist_feats, 'w') as f:
                	for feat in pool.imap(extract_hist,read_q_data,chunksize=args.chunksize):
                        	np.savetxt(f, feat, delimiter=",")

	print('Train PCA\n')
	#train pca
	#fea_file = [args.gist_feats,args.hist_feats]
	fea_file = [args.hist_feats]
	t10 = time.time()
	trained_pca = Parallel(n_jobs=num_cores)(delayed(pca_train)(fea_file[i]) for i in range(len(fea_file)))
	t11 = time.time()

	print('Extract features from images\n')
	#extract features from queries
	t20 = time.time()
	im_q_paths = Parallel(n_jobs=num_cores)(delayed(read_image_path)(read_q_data[i],'query') for i in range(len(read_q_data)))
	#q_gist_pca_des = Parallel(n_jobs=num_cores)(delayed(extract_gist_pca_features)(im_q_paths[i],trained_pca[0]) for i in range(len(im_q_paths)))
	q_hist_pca_des = Parallel(n_jobs=num_cores)(delayed(extract_hist_pca_features)(im_q_paths[i],trained_pca[0]) for i in range(len(im_q_paths)))

	#cat features
	t30 = time.time()
	#q_cat_fea = Parallel(n_jobs=num_cores)(delayed(cat_matr)(q_gist_pca_des[i],q_hist_pca_des[i]) for i in range(len(im_q_paths)))
	t31 = time.time()

	#list_to_array_fea = np.squeeze(np.array([fea for fea in q_cat_fea]))
	list_to_array_fea = np.squeeze(np.array([fea for fea in q_hist_pca_des]))

	#delete unwanted stuff to retrieve memory 
	trained_pca = None ; q_gist_pca_des = None ; q_hist_pca_des = None ;  
	
	cluster(list_to_array_fea, im_q_paths, resW)
	res_file_name.close()	

if __name__ == '__main__':
	main()
