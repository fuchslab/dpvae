import csv
import cv2
import numpy as np
from PIL import Image
from scipy.spatial import distance
import itertools
from itertools import groupby
from operator import itemgetter
import os
import argparse
import ast
import sys
import random 
import torch
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import torchvision.utils as vutils
import torchvision.transforms as transforms
import shutil
import logging
from operator import itemgetter


parser = argparse.ArgumentParser(description='Query selection')

parser.add_argument('--cluster_file','-cluster_file', metavar='PATH', help='read from file containing output of clustering')
parser.add_argument('--hist_graph','-hist_graph', metavar='PATH', help='graph of the histogram of frequency')
parser.add_argument('--cluster_tiles','-cluster_tiles', metavar='PATH', help='fig of the tiles of images from different clusters')
parser.add_argument('--result', '-res', metavar='PATH', help='file to write selected (query,label) tuples')
parser.add_argument('--samples','-samples', metavar='int', type=int, help='# samples | picked randomly')
parser.add_argument('--query','-qdir', metavar='DIR', help='path to query directory')
parser.add_argument('--move','-move', metavar='Boolean', help='move files to query dir if set to True')
parser.add_argument('--seed','-seed', metavar='int', type=int, help='specify seed to reproduce results')
parser.add_argument('--random','-random', metavar='Boolean', help='if random set to True')
parser.add_argument('--top_dist','-top_dist', metavar='Int', help='top dists')
parser.add_argument('--vis','-vis', metavar='Boolean', help='vis images belonging to cluster | if set to True')
parser.add_argument('--closest','-closest', metavar='Boolean', help='closest to the centroid | if set to True')
parser.add_argument('--closest_furthest','-closest_furthest', metavar='Boolean', help='closest and furthest to the centroid | if set to True')
logging.basicConfig(level=logging.DEBUG)

#read file
def file_read(file_to_read):
        csv.field_size_limit(sys.maxsize)
        read_file = open(file_to_read)
        reader = csv.reader(read_file)
        read_data = list(reader)
        return read_data


def query_select(cluster_labels_items, n, res_q):
	cluster_labels = [x[1] for x in cluster_labels_items]
	cluster_items = [x[0] for x in cluster_labels_items]
	cluster_dist = [x[2] for x in cluster_labels_items]

	#labels/cluster class
	y = [label for ind,label in enumerate(cluster_labels)] ; y = np.array(y)

	#get some stats along the way 
	fig1 = plt.figure(1)
	plt.title('Clustering stats: Frequency vs class')
	plt.xlabel('class'); plt.ylabel('occurences')
	plt.hist(y,bins=np.unique(y))
	fig1.savefig(args.hist_graph)
	fig1.show()
		
	classes = np.unique(cluster_labels) 
	batchims = torch.zeros(len(classes)*n,3,224,224); j=0;
	for label in range(len(classes.tolist())):
	      	indices_per_class = [i for i,x in enumerate(cluster_labels) if x==label]

		#randomly pick samples from the cluster bucket 
	        if (args.random == 'True'): 
			choice = np.random.choice(indices_per_class,n,replace=False) ;
		#non-random (based on closest points to the centroid) 	
		else:
			dist = [] ; 
			for i in range(len(indices_per_class)):
				index = indices_per_class[i]
				dist.append( tuple((index,cluster_dist[index])) )
	
			if args.closest == 'True':
				des_sort_dist = sorted(dist,key=itemgetter(1))
			else:
				des_sort_dist = sorted(dist,key=itemgetter(1),reverse=True)
				
			if args.closest_furthest == 'True':
				des_sort_dist = des_sort_dist[::len(des_sort_dist)-1]	
			choice = [x[0] for x in des_sort_dist]

                for ind in range(n):
                        choice_unit = choice[ind]
                        sample_per_choice = cluster_items[choice_unit]
			sample_dist = cluster_dist[choice_unit]
			im = Image.open(sample_per_choice) ; im = im.convert('RGB') ; im = transforms.ToTensor()(im) ;	
			im = im.view(1,im.size(0),im.size(1),im.size(2))
			batchims[j] = im
			if args.move == 'True':
				shutil.move(str(sample_per_choice), str(args.query))
			#write info to res file
			res_q.writerow([sample_per_choice,label,sample_dist])
			#mv file to query directory
			j +=1;
	
	if args.vis == 'True':
		vutils.save_image(batchims,
		    args.cluster_tiles,
		    nrow=args.samples,
                    normalize=True)

	return None	

#init file to write results
def res_file_write_init(result):
        res_file_name = result
        resFile = open(res_file_name, 'w')
        resWriter = csv.writer(resFile)

        return resWriter, resFile, res_file_name

def main():
        global args
        args = parser.parse_args()
	np.random.seed(args.seed)

	resW, res_file_name, resFile = res_file_write_init(args.result);

	cluster_labels_items_fi = file_read(args.cluster_file)
	cluster_labels_items = ast.literal_eval(cluster_labels_items_fi[0][0])
	
	query_select(cluster_labels_items,args.samples, resW)
	res_file_name.close()

if __name__ == '__main__':
        main()


 
