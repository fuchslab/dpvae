import os
import sys
import csv
import argparse
from PIL import Image
import torch
import torchvision.transforms as transforms
from torch.autograd import Variable
import numpy as np
from itertools import groupby
from operator import itemgetter
import time
import glob
import itertools
import logging

parser = argparse.ArgumentParser(description='IMR using Siamese Network')
parser.add_argument('--model_path','-m', metavar='PATH',default='/home/rajannaa/IMR_lfs/models/siam/model_2.pth.tar', type=str, help='model path to load those weights')
parser.add_argument('--query','-query', metavar='DIR',default='/home/rajannaa/IMR_lfs/dataset/lbbp/query/', help='path to query directory')
parser.add_argument('--candid','-candid', metavar='DIR',default='/home/rajannaa/IMR_lfs/dataset/lbbp/candidates/',  help='path to candidate directory')
parser.add_argument('--output', '-output',metavar='PATH',default='/home/rajannaa/IMR_lfs/', help='result dir to be written to')
parser.add_argument('--jobid','-jobid', help='job id')
parser.add_argument('--njobs','-njobs', help='n jobs')
parser.add_argument('--arch', '-a', metavar='ARCH', default='resnet18', help='model architecture:')

logging.basicConfig(level=logging.DEBUG)

def file_names(dirname):
        ext = ".png" ; pathname = os.path.join(dirname, "*" + ext)
        images_names = sorted(glob.glob(pathname))

        return images_names;

def load_image(img_id0,img_id1):
	transformation = transforms.Compose([transforms.ToTensor(), transforms.Scale transforms.Normalize((0.5, 0.5, 0.5), (0.2, 0.2, 0.2))])
	img0 = Image.open(img_id0) ; img0 = img0.convert('RGB') ; img0 = transformation(img0) ;
        img1 = Image.open(img_id1) ; img1 = img1.convert('RGB') ; img1 = transformation(img1) ;
	return img0.view(1,3,224,224), img1.view(1,3,224,224)

def model_init():
	sys.path.append('/home/rajannaa/IMR/code/dp/siameseNet/')
	from model import train_from_scratch
        model = train_from_scratch(args.arch)
        checkpoint = torch.load(args.model_path,map_location=lambda storage, loc: storage) ; #convert to cpu
        model.load_state_dict(checkpoint['state_dict'])
	return model

def extract_score(img0,img1,loaded_model):		
	img0,img1 = Variable(img0), Variable(img1)	
	output = loaded_model(img0, img1, args.arch)
	return output.data[0].numpy()

def nn_compute(items, loaded_model, file_id):
	img0, img1 = load_image(items[0], items[1])
        score = extract_score(img0, img1, loaded_model)

	file_id.writerow([items[0],items[1],score[0]])	
	
	return None

def main():
	global args
	args = parser.parse_args()
      
        qs = file_names(args.query)
        cs = file_names(args.candid)

	#loaded with pretrained weights	
	loaded_model = model_init() ;
	loaded_model.eval()

	all_q_c_pairs = list(itertools.product(qs,cs))
	logging.debug('len: {}'.format(len(all_q_c_pairs)))
	filenames = list(range(int(args.njobs)))
	f_ids = []
	for files in filenames:
		file_id = args.output + str(files) + '.csv'
		f_ids.append(file_id)

	with open(f_ids[int(args.jobid)-1],'w') as f_id:
		for i, items in enumerate(all_q_c_pairs):
            		if i % int(args.njobs) == int(args.jobid)-1:
				f = csv.writer(f_id)
				#logging.debug('output is {} and {} and {}'.format(args.jobid,items,f_ids[int(args.jobid)]))
                       		nn_compute(items, loaded_model, f)

if __name__ == '__main__':
        main()

